# [Elixir][1] study notes

## Pattern-matching `=`

    a = 1     # success

The match operator works like an assertion. It succeeds if it can make the LHS equal the RHS. __Elixir will only change the value of a variable *on the LHS*__. A variable on the RHS will be replaced by its value.

In the example above, binding the value `1` to the variable `a` will cause the match to succeed.

    list = [1, 2, 3]  # success
    [a, b, c] = list  # success

In the example above, the elements of `list` are bound to the variables `a`,`b` and `c`.

    list = [1, 2, 3]  # success
    [d, e] = list     # failure

This fails, because of a mismatch in the number of terms.

    list = [1, 2, [3, 4, 5]]  # success
    [a, b, c] = list          # success

This succeeds. `c` is bound to `[3, 4, 5]`

    list = [1, 2, 3]  # success
    [a, 2, b] = list  # success
    [a, 1, b] = list  # failure (MatchError)

No variables are set in the third match above, because the match failed.

    a..b = 1..10

The range match succeeds, with `a = 1` and `b = 10`

### More examples

    a = [1, 2, 3]           # success
    a=4                     # success
    4=a                     # success
    [a, b] = [ 1, 2, 3 ]    # failure
    a = [ [ 1, 2, 3 ] ]     # success
    [ a..5 ] = [ 1..5 ]     # success
    [a] = [ [ 1, 2, 3 ] ]   # success
    [[a]] = [ [ 1, 2, 3 ] ] # failure


* Ignore a value with `_`

        [_, a, _] = [1, 2, 3]   # success

* Prevent reassignment a variable in a match with `^`

        [a, b, c] = [1, 2, 3]   # success
        [1, 2, ^a] = [1, 2, 3]  # failure

* Variables assign once per match

        [a, a] = [1, 1]   # success
        [a, a] = [1, 2]   # failure

        a = 2             # success
        ^a = 0 + a        # success

## Types

### Built-in types

* Value types:
    * Arbitrary-sized integers
    * Floating point numbers
    * Atoms
    * Regular expressions
    * Ranges (? see p23,26)
* System types:
    * PIDs and Ports
    * References
* Collection types
    * Lists
    * Tuples
    * Binaries

#### Value types

##### Integers

Integers have no fixed size limit

    1234, 1_234_567     # integer literals
    0x12af              # hexadecimal
    0765                # octal
    0b100110110         # binary

##### Floating point numbers

[IEEE 754 double precision][2]  - 15-17 decimal digits precision, exponent  +/-308

    1.0
    1.234
    123.4567e-123

##### Atoms

Atoms are constants that represent the name of something. Their name is their value. Similar to Ruby's _symbols_. Written in [PCRE][3]-ish format as:

    atom        => [:][atom-word | elixir-operator | "arbitrary-characters"]
    atom-word   => [a-zA-Z0-9_@]+[!?]? 

Two atoms with the same name compare as equal

    :a_legal_atom? = :a_legal_atom?     # success

Examples:

    :_abc
    :+
    :a_legal_atom?
    :another_one!
    :_ab@123
    :"an arbitrary collection of characters !@<>{}[]-+~'=-*&"

##### Regular expressions

Literals written as: 

    %r[delimiter]regexp[matching-delimiter][opt]*
    deimiter = [^a-zA-Z0-9] excluding regex special characters
    opt = [fgimrsux]
    * f: the pattern must start to match on the first line of a multiline string
* g: support named groups
* i: make matches case insensitive
* m: if the string to be matched contains multiple lines, ^ and $ match the start and end of these lines. \A and \z continue to match the beginning or end of the string
* r: normally modifiers like * and + are greedy, matching as much as possible. The r modifier makes them reluctant, matching as little as possible
* s: allows . to match any newline characters
* u: enable unicode specific patterns like \p
* x: extended mode—ignore whitespace and comments (# to end of line)

Examples

    %r/[aeiou]/
    %r{[aeiou]}
    %r<[aeiou]>
    %r@[aeiou]@
    %r'[aeiou]'
    %r/[Aeiou.]/si
    %r/The cat (s[.]*at) on the mat/g

Use the `Regex` module to manipulate regular expressions. It contains methods such as `run`, `scan`, `split`, `replace`.

    Regex.replace %r/[aeiou]/, "alphabet", "_"

##### Ranges

Ranges are written as `a..b`, where `a` and `b` can be any similar or dissimilar type, but must be integers to be iterable.

    1..5
    "alpha".."omega"
    1..2.345
    "alpha"..[1,2,3]

#### System types

Resources in the Erlang VM

##### Pids and Ports
* A _pid_ is a reference to a local or remote process
* A _port_ is a reference to a resource, typically external, that you read or write.

##### References
* `make_ref` creates a globally-unique reference. Obscure.

#### Collections

* Elixir collections can hold elements of _any_ type, including other collections.

##### Tuples

* An ordered collection of elements
    * immutable after creation
    * typically 2-4 elements
    * use _records_ for more elements
    * write as a brace-delimited comma-separated list of elements
    * can be used in pattern-matching

            { 1, 2 }
            { :ok, 42, "next" }
            { a, b } = { 1, "next" }     # success

* An Elixir _idiom_ is for functions to return a tuple where the first element is the atom `:ok` on success

        iex> {status, file} = File.open("Rakefile")
        {:ok, #PID<0.39.0>}

* Another _idiom_ is to write _matches_ that assume success:

        { :ok, file } = File.open("Rakefile")

##### Lists

Created as a pair of square brackets `[]` containing 0 or more comma-separated items:

    []
    [1, 2, 3]
    ["a", 1, ["b", 2.3]]

* Special operators

        [ 1, 2, 3 ] ++ [ 4, 5, 6 ]      # concatenation
        [ 1, 2, 3, 4, 5 ] -- [ 2, 3 ]   # difference
        1 in [ 1, 2, 3, 4 ]             # membership (boolean expression)

* Keyword lists

        [ subject: "dog", verb: "chases", object: "cat" ]

    * single space between colon `:` and value is _necessary_

    * converted by Elixir to a list of two-element symbol-string tuples

            [ {:subject, "dog"}, {:verb, "chases"}, {:object, "cat"} ]

    * square-brackets can be omitted if a keyword list is the last argument to a function call

            DB.save record, use_transaction: true, logging: "HIGH"

        is equivalent to:

            DB.save record, [ {:use_transaction, true}, {:logging, "HIGH"} ]

    * You can also leave off the brackets if a keyword list appears as the last item in any context where a list of values is expectedener

            [1.23, eat: 1, more: 2, vegetables: 3]

### Other types

* Strings, Ranges and Records are found in Elixir, but are composed from the built-in types
* Functions are a type

* Single-quoted strings are a list of character-codes

        'abc' == [97,98,99]


## Anonymous functions

        fn {arg-list} -> {function body} end

In these examples, the anonymous function is matched to a variable named `add`

        add = fn(x,y) -> x+y end   # 1st form (parentheses around arguments can be omitted)
        add = &(&1 + &2)           # 2nd form

* Anonymous functions have an unusual calling syntax:

        add.(1,2)

Note the period `.` between the function variable and its arguments

* Library or module functions can be assigned to function variables:

        my_flatten = &List.flatten/1

Note that the ampersand appears before the function and the 'arity' is specified

## Modules

* Module names must be initially capitalised 
* Provide a lexical scope for contained functions and data

        defmodule ModuleName do
        .....
        end

* Modules can be nested

        defmodule Outer do
          defmodule Inner do
          end
        end

    or...

        defmodule Outer.Inner do
        ...
        end

### Functions

## List comprehensions

        {value} = lc [generator|filter]+, do: {expression}
        {generator} = {pattern} inlist {list}
        {generator} = {pattern} inbin {binary}

* Generator and filters are nested left-to-right

An example with two generators. The generator for 'y' is nested inside the generator for 'x'

        iex> lc x inlist [1,2], y inlist [5,6], do: x * y
        [5, 6, 10, 12]

An example with a generator and a filter

        iex> lc x inlist [1,2,3,4,5,6], rem(x,2) == 0, do: x
        [2,4,6]


[1]: http://elixir-lang.org
[2]: http://en.wikipedia.org/wiki/Double-precision_floating-point_format
[3]: http://pcre.org
