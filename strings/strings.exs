defmodule MyStrings do

  @printables Enum.to_list ? ..?~

  def is_printable(s), do: _is_printable(s, [])
  defp _is_printable([], []), do: false
  defp _is_printable([], _), do: true
  defp _is_printable([h|t], cl) do
    if h in @printables do
      _is_printable(t, cl ++ [h])
    else
      false
    end
  end

  def anagram(a,b), do: Enum.sort(a) == Enum.sort(b)

  def eval([]), do: :error
  def eval(expr) do
    [al, opl, bl] = char_list_split(expr, ' ')
    a = list_to_integer(al)
    b = list_to_integer(bl)
    case opl do
      '+' -> a + b 
      '-' -> a - b
      '/' -> a / b
      '*' -> a * b
    end
  end

  def char_list_split(cl, split_char) do
    is_split_char = &(&1 == hd(split_char))
    has_split_char = &(hd(split_char) in &1)
    Enum.chunks_by(cl, is_split_char) |> Enum.reject(has_split_char)
  end

end
