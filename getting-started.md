# ![Droplet][0] Getting started with [Elixir][1]

## Text
* [Getting Started][2]

## Videos
* [Nine minutes of Elixir][3] - PragDave (9 mins)
* [Introduction to Elixir][4] - PragDave (30 mins)

## Reference
* [Elixir standard library][5]
* [Elixir packages][6]
* [Erlang standard library][7]
* [Erlang OTP guide][8]

[0]: ./drop.png
[1]: http://elixir-lang.org/
[2]: http://elixir-lang.org/getting_started/1.html
[3]: http://www.youtube.com/watch?v=hht9s6nAAx8
[4]: http://www.youtube.com/watch?v=a-off4Vznjs
[5]: http://elixir-lang.org/docs/stable/
[6]: http://expm.co/
[7]: http://www.erlang.org/doc/apps/stdlib/index.html
[8]: http://www.erlang.org/doc/design_principles/users_guide.html