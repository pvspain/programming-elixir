fizzbuzz = fn 
  0,0,_ -> "FizzBuzz"
  0,_,_ -> "Fizz"
  _,0,_ -> "Buzz"
  _,_,c -> "#{c}"
end
# IO.puts fizzbuzz.(0,0,0)
# IO.puts fizzbuzz.(0,1,2)
# IO.puts fizzbuzz.(1,0,2)
# IO.puts fizzbuzz.(1,2,3)

fizzbuzz2 = fn 
  n -> fizzbuzz.(rem(n,3), rem(n,5), n)
end

IO.puts fizzbuzz2.(10)
IO.puts fizzbuzz2.(11)
IO.puts fizzbuzz2.(12)
IO.puts fizzbuzz2.(13)
IO.puts fizzbuzz2.(14)
IO.puts fizzbuzz2.(15)
IO.puts fizzbuzz2.(16)

