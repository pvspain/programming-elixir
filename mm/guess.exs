defmodule Chop do
  def guess(actual,range) do
    lo..hi = range
    guess_helper(div(lo+hi,2), actual, lo, hi)
  end

  def guess_helper(_g, actual, lo, hi) when actual < lo or actual > hi do 
    IO.puts "#{actual} is not in range #{lo}..#{hi}"
  end

  def guess_helper(g, actual, _lo, hi) when g < actual do
    IO.puts "Is it #{g}"
    guess_helper(div(g+1+hi,2), actual, g+1, hi)
  end
 
  def guess_helper(g, actual, lo, _hi) when g > actual do
    IO.puts "Is it #{g}"
    guess_helper(div(lo+g-1,2), actual,lo, g-1)
  end
  
  def guess_helper(g, actual, _lo, _hi) when g == actual do
    IO.puts "#{g}"
  end
end
