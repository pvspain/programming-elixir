defmodule Prime do

  def span(a,b) when a < b, do: [a | span(a + 1, b)]
  def span(a,b) when a > b, do: []
  def span(a,a), do: [a]
  
  def primes2to(n), do: _primes(span(2,n), [])
  defp _primes([], pl), do: pl
  defp _primes([nh|nt], []), do: _primes(nt, [nh]) 
  defp _primes(nl, [ph|pt]) do
    [ph | _primes((lc n inlist nl, rem(n, ph) != 0, do: n), pt)]
  end 

end
