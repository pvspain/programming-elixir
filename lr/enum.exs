defmodule MyEnum do
  
  def all?([], _f), do: false
  def all?(l, f), do: _all?(l, f, true)
  defp _all?([], _f, result),  do: result
  defp _all?([h|t], f, true), do: _all?(t, f, f.(h))
  defp _all?(_l, _f, false), do: false

  def each([], _f), do: :ok
  def each([h|t], f) do 
    f.(h)
    each(t, f)
  end

  def filter(l, f), do: (lc x inlist l, f.(x), do: x)

  def split(l, n), do: _split([], l, n) 
  defp _split(s, r, 0), do: {s, r}
  defp _split(s, [], _n), do: {s, []}
  defp _split([s], [h|t], n), do: _split([s|h], t, n-1)

  def take([], _n), do: []
  def take(_l, 0), do: []
  def take([h|t], n), do: [h | take(t, n-1)]
  
end

