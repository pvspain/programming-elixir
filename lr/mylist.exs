defmodule MyList do

  def len([]), do: 0
  def len([_head | tail]), do: 1 + len(tail)

  def square([]), do: []
  def square([head | tail]), do: [head*head | square(tail)]

  def add_1([]), do: []
  def add_1([head | tail]), do: [ head + 1 | add_1(tail) ]

  def map(_f, []), do: []
  def map(f, [h | t]), do: [f.(h) | map(f, t)] 

  def sum([]), do: 0
  def sum([h|t]), do: h + sum(t)

  def reduce([], init, _f), do: init
  def reduce([h|t], init, f), do: reduce(t, f.(init, h), f) 

  def mapsum([], _f), do: 0
  def mapsum([h|t], f), do: f.(h) + mapsum(t, f)

  def max([h|t]), do: _max(t, h)
  defp _max([], m), do: m
  defp _max([h|t], m) when h > m, do: _max(t, h)
  defp _max([h|t], m) when h < m, do: _max(t, m)

  def caesar([], _n), do: []
  def caesar(l, n), do: _caesar(l, rem(n,26), [])
  defp _caesar([], _n, txlist), do: txlist
  defp _caesar([h|t], n, tx) when [h+n] > 'z', do: _caesar(t, n, tx++[h + n - 26])
  defp _caesar([h|t], n, tx), do: _caesar(t, n, tx++[h + n])

  def span(a,b) when a < b, do: [a | span(a + 1, b)]
  def span(a,b) when a > b, do: []
  def span(a,a), do: [a]

  def flatten([]), do: []
  def flatten([h|t]) when not is_list(h), do: [h|flatten(t)] 
  def flatten([h|t]) when is_list(h), do: flatten(h)++flatten(t)
end
