defmodule C do

# argument func is unused below -> add leading underscore to suppress warning
  def map([], _func), do: []
  def map([head|tail], func), do: [func.(head) | map(tail, func)]

end

IO.inspect C.map [1,2,3,4,5], fn x -> x*x end
IO.inspect C.map [1,2,3,4,5], &1*&1